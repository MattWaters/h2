﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace h2.OrbitalBodies
{
    class Galaxy
    {
        public string Name { get; }
        public Dictionary<string, Sector> Sectors { get; set; } = new Dictionary<string, Sector>();
        public Galaxy(XDocument inputXml)
        {
            // Disabling PossibleNullReferenceException check here as validation will be done before passing to this function.
            // ReSharper disable PossibleNullReferenceException
            Name = inputXml.Root.Element("galaxy").Attribute("name").Value;

            foreach (XElement sector in inputXml.Root.Element("galaxy").Elements())
            {
                Sector newSector = new Sector(
                    sector.Attribute("sectorId").Value,
                    sector.Attribute("name").Value,
                    int.Parse(sector.Attribute("x").Value),
                    int.Parse(sector.Attribute("y").Value),
                    int.Parse(sector.Attribute("z").Value),
                    sector.Elements()
                );

                Sectors.Add(newSector.Id, newSector);
            }
        }
    }
}
