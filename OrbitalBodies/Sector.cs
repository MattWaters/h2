﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace h2.OrbitalBodies
{
    class Sector
    {
        public string Id { get; }
        public string Name { get; }
        public int X { get; }
        public int Y { get; }
        public int Z { get; }
        public Dictionary<string, System> Systems { get; set; } = new Dictionary<string, System>();

        public Sector(string id, string name, int x, int y, int z, IEnumerable<XElement> systems)
        {
            Id = id;
            Name = name;
            X = x;
            Y = y;
            Z = z;

            foreach (XElement system in systems)
            {
                System newSystem = new System(
                    system.Attribute("systemId").Value,
                    system.Attribute("name").Value,
                    float.Parse(system.Attribute("x").Value),
                    float.Parse(system.Attribute("y").Value),
                    float.Parse(system.Attribute("z").Value),
                    system.Attribute("eod").Value
                );

                Systems.Add(newSystem.Id, newSystem);
            }
        }
    }
}
