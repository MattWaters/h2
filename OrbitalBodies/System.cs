﻿namespace h2.OrbitalBodies
{
    internal class System
    {
        private readonly string _status;

        public System(string id, string name, float x, float y, float z, string status)
        {
            Id = id;
            Name = name;
            X = x;
            Y = y;
            Z = z;
            _status = status;
        }

        public string Id { get; }
        public string Name { get; }
        public float X { get; }
        public float Y { get; }
        public float Z { get; }
        public bool Explored => this._status != "Unexplored";
    }
}
