﻿using System;
using System.IO;
using System.Windows;
using System.Xml.Linq;
using h2.OrbitalBodies;
using Microsoft.Win32;

namespace h2
{
    public partial class MainWindow : Window
    {
        private Galaxy galaxy;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenFile(object sender, RoutedEventArgs e)
        {
            string[] paths =
            {
                Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                "Shores of Hazeron\\Programs"
            };

            string defaultFolderPath = Path.Combine(paths);

            OpenFileDialog openFileDialog = new OpenFileDialog
                {Filter = "XML Files (*.xml)|*.xml", InitialDirectory = defaultFolderPath};

            if (openFileDialog.ShowDialog() == true)
            {
                XDocument xml = XDocument.Load(@openFileDialog.FileName);

                if (xml.Root.Attribute("empire") != null)
                {
                    Title = xml.Root.Attribute("empire").Value;
                    galaxy = new Galaxy(xml);

                    foreach (var sector in galaxy.Sectors.Values)
                    {
                        treeView.Items.Add(sector.Id);
                    }
                }
            }
        }
    }
}
